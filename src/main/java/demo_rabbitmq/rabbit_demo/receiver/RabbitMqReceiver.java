package demo_rabbitmq.rabbit_demo.receiver;


import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

/**
 * @author Tomas Kloucek
 */
@RabbitListener(queues = {"fruits", "vegetables"})
public class RabbitMqReceiver {

	@RabbitHandler
	public void receive(String in) {
		System.out.println(" [x] Received '" + in + "'");
	}

}
