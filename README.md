# AMQP with Spring Framework and RabbitMQ #

In the company where I work, we're considering to use RabbitMQ message broker for integrations between systems. RabbitMQ is based on AMQP protocol. First, let's shed some light at what AMPQ is compare to traditional JMS.

AMPQ vs JMS:
-------------
* AMPQ is messaging protocol defined at the level of wire, JMS is an API.

* JMS has queues and topics as targets for publishing messages, AMPQ has only queues and exchanges. AMPQ sender always sends messages into exchange only. Exchange eventually takes care of the routing to connected queues based on configured routing strategy. Take exchange as something like service bus. 

* AMPQ resources are all defined with AMPQ protocol, so protocol library can define whole infrastructure!

* In JMS you can use two message patterns P-2-P or publish-subscribe. With AMPQ you've got four options:

1. Direct routing - message is routed based on routing key, no wildcard support.
2. Topic routing - same as Direct, but with wildcard support.
3. Fanout routing - message is routed to every queue binded to exchange.
4. Headers routing - message is routed based on value in the message header.

## RabbitMQ installation ##

If you're on the Mac like me then simply follow instructions on this page:

[RabbitMQ installation on MacOS](https://www.rabbitmq.com/install-homebrew.html)

## RabbitMQ demo ##

After starting the broker with **rabbitmq-server.sh**, first type into your browser 

```
http://localhost:15672
```
and create **direct routing** based exchange with name **"test_exchange"** and two queues with names **"fruits"** and **"vegetables"**. After that, **connect these two queues to the test_exchange** with routing keys "fruits" in case of fruits queue and "vegetables" in case of vegetables queue. Don't forget to verify the settings with perfect and handy Rabbit visualizer. 

## RabbitMQ sender ##

Let's take a look on how sender could look like. First sender configuration:


```
package demo_rabbitmq.rabbit_demo.configuration;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo_rabbitmq.rabbit_demo.annotations.Sender;
import demo_rabbitmq.rabbit_demo.sender.RabbitMqSender;

@Configuration
@Sender
public class RabbitMqConfiguration {
	{
		System.out.println("Creating sender config...");
	}
	
	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = 
				new CachingConnectionFactory("localhost");
		connectionFactory.setUsername("guest");
		connectionFactory.setPassword("guest");
		return connectionFactory;
	}
	
	@Bean
	public AmqpAdmin amqpAdmin() {
		return new RabbitAdmin(connectionFactory());
	}

	@Bean
	public RabbitTemplate rabbitTemplate() {
		RabbitTemplate template = new RabbitTemplate(connectionFactory());
		template.setExchange("test_exchange");
		return template;
	}

	@Bean(initMethod="send")
	public RabbitMqSender sender() {
		final RabbitMqSender sender = new RabbitMqSender();
		return sender;
	}
}
```
* Notice the RabbitTemplate configuration how we setup previously created entry point, exchange **test_exchange**. Now everything this template will publish is going to be send into test_change.

* AmpqAdmin is not necessary to declare, but this bean gives you a chance for declaring complete infrastructure. Bindings, queues, exchanges...

* CachingConnectionFactory creates own connection to the RabbitMq broker, by default under guest/guest credentials. You can create other users either under the console or with powerful tool **rabbitmqctl**.

Code for sender:

```
package demo_rabbitmq.rabbit_demo.sender;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Tomas Kloucek
 */
public class RabbitMqSender {

	@Autowired
	private RabbitTemplate template;
	
	public void send() {
		// fruits
		this.template.convertAndSend("fruits", "Orange");
		this.template.convertAndSend("fruits", "Banana");
		this.template.convertAndSend("fruits", "Apricotes");
		// vegetables
		this.template.convertAndSend("vegetables", "xxx1");
		this.template.convertAndSend("vegetables", "xxx2");
		this.template.convertAndSend("vegetables", "xxx3");
	}
}
```

pretty straightforward I think. It means that text messages Orange, Banana and Apricots will be routed based on the "fruits" key into fruits queue. Same with the vegetables.

## RabbitMq receiver ##

How about when we want to receive from previously created queues. Again, first code configuration:

```
package demo_rabbitmq.rabbit_demo.configuration;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo_rabbitmq.rabbit_demo.annotations.Receiver;
import demo_rabbitmq.rabbit_demo.receiver.RabbitMqReceiver;

@Receiver
@Configuration
public class RabbitMqReceiveConfiguration {
	
	{
		System.out.println("Creating receiving configuration.");
	}
	
	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = 
				new CachingConnectionFactory("localhost");
		connectionFactory.setUsername("guest");
		connectionFactory.setPassword("guest");
		return connectionFactory;
	}
	
	@Bean
	public AmqpAdmin amqpAdmin() {
		return new RabbitAdmin(connectionFactory());
	}
	
	@Bean
	public Queue fruits() {
		return new Queue("fruits");
	}
	
	@Bean
	public Queue vegetables() {
		return new Queue("vegetables");
	}
	
	@Bean
	public RabbitMqReceiver receiver() {
		return new RabbitMqReceiver();
	}
}
```
Notice how we created references to queues fruits and vegetables. Now let's create the event based receiver:


```
package demo_rabbitmq.rabbit_demo.receiver;


import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

/**
 * @author Tomas Kloucek
 */
@RabbitListener(queues = {"fruits", "vegetables"})
public class RabbitMqReceiver {

	@RabbitHandler
	public void receive(String in) {
		System.out.println(" [x] Received '" + in + "'");
	}

}
```

## Howto test the demo ##

* run **mvn clean install**
* **java -jar -Dspring.profiles.active=sender target/demo-0.0.1-SNAPSHOT.jar** will launch the sender
* **java -jar -Dspring.profiles.active=receiver target/demo-0.0.1-SNAPSHOT.jar**

when launching app as receiver, you should see the following output:


```
 [x] Received 'Orange'
 [x] Received 'xxx1'
 [x] Received 'Banana'
 [x] Received 'xxx2'
 [x] Received 'Appricotes'
 [x] Received 'xxx3'
```

And guys that's it. Later I plan to show you how acknowledgement works with rabbitmq compare to JMS and other things. 

cheers

Tomas